local pickers = require 'telescope.pickers'
local finders = require 'telescope.finders'
local conf = require 'telescope.config'.values
local actions = require 'telescope.actions'
local action_state = require 'telescope.actions.state'

local extension = { exports = {} }

extension.setup = function(config)
  if config.theme then
    extension.config = require "telescope.themes"['get_' .. config.theme] { config }
  else
    extension.config = require "telescope.themes".get_dropdown { config }
  end

  extension.config.action = config.action or vim.fn.system
end

extension.exports.just = function(opts)
  opts = vim.tbl_deep_extend("force", extension.config, opts or {})

  local picker = pickers.new(opts, {
    prompt_title = "just ...",
    finder = finders.new_oneshot_job(
      { "just", "--list", "--list-heading", "", "--list-prefix", "" },
      opts
    ),
    sorter = conf.generic_sorter(opts),
    attach_mappings = function(bufnr)
      actions.select_default:replace(function()
        actions.close(bufnr)
        local recipe = action_state.get_selected_entry()[1]
        local command = "just " .. vim.loop.cwd() .. "/" .. recipe
        opts.action(command)
      end)
      return true
    end
  })

  picker:find()
end


return require("telescope").register_extension(extension)
