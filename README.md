# telescope-just.nvim

<!--toc:start-->
- [telescope-just.nvim](#telescope-justnvim)
  - [Installation](#installation)
  - [Usage](#usage)
<!--toc:end-->

This is a [Telescope](https://github.com/nvim-telescope/telescope.nvim) picker 
for [just](https://github.com/casey/just) recipes.

## Installation

Use your package manager of choice. For [lazy.nvim](https://github.com/folke/lazy.nvim):
```lua
{
  url = "https://codeberg.org/elfahor/telescope-just.nvim",
  requires = {
    "nvim-telescope/telescope.nvim",
  },
}
```
Then, somewhere after your call to `require 'telescope'.setup()`, put:

```lua
require 'telescope'.load_extension 'just'
```
## Usage

It is recommended that you use the dropdown theme. Indeed, there is no preview, so there is
no need to have the window be super large. It is enabled by default.

The Telescope picker returns the shell command to execute in order to run the 
selected recipe.
It is up to you to decide what to do with this command. By default, it is simply ran
synchronously using `vim.fn.system`.

You can decide what to do with it by passing an `action` parameter to Telescope's setup
function. Example:
```lua
require 'telescope'.setup {
  extensions = {
    just = {
      -- I rather suggest dropdown!
      theme = 'ivy',
      -- A good option is to show a popup window.
      -- You can do that with tmux or toggleterm.
      action = function(command)
        vim.fn.system(command)
        print("Executed", command)
      end
    }
  }
}
```

And then you can call `:Telescope just`!
